import { React, Display, Nav } from './imports';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Display />
        <Nav />
      </header>
    </div>
  );
}

export default App;
