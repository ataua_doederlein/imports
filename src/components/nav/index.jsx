import {
  React,
  Button,
  styled,
  decreaseAction,
  resetAction,
  increaseAction,
  useDispatch,
} from '../../imports';

const Nav = () => {
  const dispatch = useDispatch();

  const decrease = () => dispatch(decreaseAction());
  const reset = () => dispatch(resetAction());
  const increase = () => dispatch(increaseAction());
  return (
    <div>
      <ButtonLeft label="Decrease" onClick={() => decrease()} />
      <ButtonCenter label="Reset" onClick={() => reset()} />
      <ButtonRight label="Increase" onClick={() => increase()} />
    </div>
  );
};

const ButtonLeft = styled(Button)`
  border-radius: 20px 0 0 20px;
`;
const ButtonCenter = styled(Button)`
  margin: 0 12px;
`;
const ButtonRight = styled(Button)`
  border-radius: 0 20px 20px 0;
`;

export default Nav;
