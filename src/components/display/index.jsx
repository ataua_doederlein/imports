import { React, useSelector, styled } from '../../imports';

const Display = () => {
  const count = useSelector((state) => state);

  return <DivDisplay>{count}</DivDisplay>;
};

const DivDisplay = styled.div`
  margin: 40px;
  display: grid;
  place-items: center;
  width: 320px;
  height: 180px;
  background: #ff4;
  color: #f38;
  border-radius: 20px;
  font-size: 5rem;
`;

export default Display;
