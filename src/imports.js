import { Button } from 'primereact/button';
import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import styled from 'styled-components';

import Display from './components/display';
import Nav from './components/nav';
import {
  decreaseAction,
  resetAction,
  increaseAction,
  DECREASE,
  RESET,
  INCREASE,
} from './redux/actions';
import counter from './redux/reducers';
import store from './redux/store';

export {
  React,
  thunk,
  ReactDOM,
  useState,
  useEffect,
  useDispatch,
  Button,
  DECREASE,
  RESET,
  INCREASE,
  decreaseAction,
  resetAction,
  increaseAction,
  counter,
  createStore,
  applyMiddleware,
  styled,
  Display,
  Nav,
  Provider,
  store,
  useSelector,
};
