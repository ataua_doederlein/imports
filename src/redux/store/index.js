import { counter, createStore, thunk, applyMiddleware } from '../../imports';

const store = createStore(counter, applyMiddleware(thunk));

export default store;
