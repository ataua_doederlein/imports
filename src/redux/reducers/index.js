import { DECREASE, RESET, INCREASE } from '../../imports';

const counter = (state = 0, action) => {
  const { type } = action;
  switch (type) {
    case DECREASE:
      return state - 1;
    case RESET:
      return 0;
    case INCREASE:
      return state + 1;
    default:
      return state;
  }
};

export default counter;
