const DECREASE = 'DECREASE';
const RESET = 'RESET';
const INCREASE = 'INCREASE';

const decreaseAction = () => ({
  type: DECREASE,
});
const resetAction = () => ({
  type: RESET,
});
const increaseAction = () => ({
  type: INCREASE,
});

export { decreaseAction, resetAction, increaseAction, DECREASE, RESET, INCREASE };
