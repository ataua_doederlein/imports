# Imports

This is a very basic project developed to test a functionality that popped in my head when developing a react project:

## The problem --

There are so many different imports and exports depending on how many libraries we get to use, and eventually we get confused about which one exports what. I eventually import, for example, useHistory from react (yes, I am a little dumb...), or useSelector from redux (what can I do? I have ADHD...), just to see the application crash and fix it according to the error messages.
How can I fix it?

## The solution --

What if I import everything only once to the same single place, and then export from there? That way, on every component I wish to use any of those imports, I would import whatever I wanted from there - a 'helper.js' file or such...

So I've created a file called `imports.js` and centralized everything there. Now let's test it...

That actually worked! - except for the hooks, that have to be instantiated from within components (I've tryed to make `const counter = useState(state => state)` and then export `counter`, but it didn't work).

Another thing that did not work was to initiate the `redux/action` constants inside the `imports` file and import from there, but for some reason it seems like those constants should have been there before anything else. So I did define them on `redux/actions` and export from there to `imports` and then to `redux/reducers`, because YEAH! I want everything to be exported from there!! Call me a dictator, I don't mind!

It worked. It is clean, it is good, it is organized, it is even beautiful. Now we are all happy. Life is good.

If you did like that, please buy me a beer when we eventually cross each other on the street.
